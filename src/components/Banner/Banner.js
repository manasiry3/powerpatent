import React from "react";
import { useBannerQuery } from "../../hooks/useBannerQuery";
import { Wrapper } from "../Banner/Banner.styles";
import ThreeSection from "../ThreeSection/ThreeSection";

const Banner = () => {
    const { banner: data } = useBannerQuery();

    return (
        <Wrapper className="banner pt-5">
            <div className="container">
                <div className="row pt-3 pb-5">
                    <div className="col-md-4 align-self-center">
                        <article>
                            <h1>{data.bannerVideo.bannerheading}</h1>
                            <p>
                                {data.bannerVideo.bannerDescriptipn}
                            </p>
                        </article>
                    </div>
                    <div className="col-md-8 align-self-center">
                        <video src={data.bannerVideo.videobanner} controls width="100%"></video>
                    </div>
                </div>
            </div>
            <ThreeSection></ThreeSection>
        </Wrapper>
    )
}
export default Banner

