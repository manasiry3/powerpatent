import { useStaticQuery, graphql } from "gatsby"

export const useCTAareaQuery = () => {
  const data = useStaticQuery(graphql`
    query CTAQuery{
      cta: wpPage(title: {eq: "Home"}) {
        bannerVideo {
          founders
          lawyers
          companies
          companiesdescription
          foundersdescription
          lawyersdescription
          companiesimage {
            id
            localFile {
              absolutePath
              childImageSharp {
                fluid {
                  src
                  srcSet
                  sizes
                  base64
                  aspectRatio
                }
              }
            }
          }
          foundersimage {
            localFile {
              absolutePath
              childImageSharp {
                fluid {
                  src
                  srcSet
                  sizes
                  base64
                  aspectRatio
                }
              }
            }
          }
          lawyersimage {
            localFile {
              absolutePath
              childImageSharp {
                fluid {
                  src
                  srcSet
                  sizes
                  base64
                  aspectRatio
                }
              }
            }
          }
        }
      }
    }
  `)

  return data;
}

