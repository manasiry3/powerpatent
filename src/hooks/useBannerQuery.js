import { useStaticQuery, graphql } from "gatsby"

export const useBannerQuery = () => {
  const data = useStaticQuery(graphql`
    query BannerQuery{
        banner: wpPage(title: {eq: "Home"}) {
        bannerVideo {
          bannerDescriptipn
          bannerheading
          fieldGroupName
          videobanner
        }
        title
        id
        uri
      }
    }
  `)
  return data;
}

