import React from 'react';
import Banner from '../components/Banner/Banner';
import Layout from '../components/Layout/Layout';
import SEO from "../components/SEO/SEO"
import './index.css'

const IndexPage = () => (
  <Layout>
    <SEO title="Home" />
    <Banner />
  </Layout>
);

export default IndexPage;
